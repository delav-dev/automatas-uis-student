---
# Proyectos 2020-1. Autómatas y lenguajes formales. 

## Prof: Gustavo Garzón

---

# Lista de Proyectos
1. [PyLex - Análisis gramático de expresiones matemáticas en Python a LaTeX y viceversa](#proy1)
2. [Traducción de Letras y Números a Código Morse y viceversa, con Representación Auditiva](#proy2)
3. [Autómata finito determinista orientado al procesamiento del yogur](#proy3)
4. [Modelado del juego Sprout por medio de un Autómata Finito Determinista](#proy4)
5. [Máquina de Turing para la detección del tipo de hábitos de una persona](#proy5)
6. [AUTÓMATA FINITO DETERMINISTA PARA LA SOLUCIÓN DEL JUEGO TORRE DE HANOI](#proy6)
7. [Biosíntesis de Turing](#proy7)
8. [Py-Kids](#proy8)
9. [Diseño de laberintos a partir de autómatas deterministas](#proy9)
10. [MODELADO DE RUTAS PRESTADAS POR LA EMPRESA DE TRANSPORTE INTERMUNICIPAL BERLINAS DEL FONCE DESDE LA TERMINAL DE TRANSPORTES DE LA CIUDAD DE BUCARAMANGA](#proy10)
11. [AFD para la simulación de un cajero electrónico](#proy11)
12. [Maquina expendedoras de cerveza](#proy12)
13. [AFD QUE CLASIFICA EL TIPO DE ETS SEGÚN SÍNTOMAS](#proy13)
14. [LINEA DE ATENCION AL CLIENTE PARA SERVICIOS MOVILES](#proy14)
15. [Automata de seguridad para conjunto residencial](#proy15)

---

## PyLex - Análisis gramático de expresiones matemáticas en Python a LaTeX y viceversa <a name="proy1"></a>

**Autores:**
**Edward Parada, Jose Silva, Yuri García**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2182070-2183075-2182697.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2182070-2183075-2182697/2182070-2183075-2182697.ipynb) [(video)](https://www.youtube.com/watch?v=aodcB0wNkPA ) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2182070-2183075-2182697.pdf)

---

## Traducción de Letras y Números a Código Morse y viceversa, con Representación Auditiva <a name="proy2"></a>

**Autores:**
**Carvajal Esparza Juan Sebastian, Cerinza Zaraza Juan Pablo, Gualdron Hurtado Yesid Romario**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2143162-2190081-2190052.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2143162-2190081-2190052/2143162-2190081-2190052.ipynb) [(video)](https://www.youtube.com/watch?v=zCmvcUKIUcA) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2143162-2190081-2190052.pdf)

---

## Autómata finito determinista orientado al procesamiento del yogur <a name="proy3"></a>

**Autores:**
**Juan Sebastián Mora Rueda, Jhan Eduardo Rojas Niño, Juan José Bayona Sepúlveda**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2181960-2182690-2183200.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2181960-2182690-2183200/2181960-2182690-2183200.ipynb) [(video)](https://www.youtube.com/watch?v=wUJOHwCENGo) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2181960-2182690-2183200.pdf)

---

## Modelado del juego Sprout por medio de un Autómata Finito Determinista <a name="proy4"></a>

**Autores:**
**Daniel David Delgado Cervantes, Valentina Galvis Bersgneider, Laura Alexandra Hernandez Perez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2182066-2182038-2182054.gif" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2182066-2182038-2182054/2182066-2182038-2182054.ipynb) [(video)](https://www.youtube.com/watch?v=VqEBlmg97yc) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2182066-2182038-2182054.pdf)

---

## Máquina de Turing para la detección del tipo de hábitos de una persona <a name="proy5"></a>

**Autores:**
**Javier Andrés Torres Quintero, Gianfranco Estevez Ruiz, Alejandro Romero Serrano**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2182058-2183074-2182059.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2182058-2183074-2182059/2182058-2183074-2182059.ipynb) [(video)](https://www.youtube.com/watch?v=wUJOHwCENGo) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2182058-2183074-2182059.pdf)

---

## AUTÓMATA FINITO DETERMINISTA PARA LA SOLUCIÓN DEL JUEGO TORRE DE HANOI <a name="proy6"></a>

**Autores:**
**Brayan Sneider Daza Suarez, Santiago Ariza Briceño**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2183223-2190040.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2183223-2190040/2183223-2190040.ipynb) [(video)](https://www.youtube.com/watch?v=aWxe-5289P8) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2183223-2190040.pdf)

---

## Biosíntesis de Turing <a name="proy7"></a>

**Autores:**
**Yezith Fernando Rincón Guevara, Daniel Alejandro León Ortiz**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2181686-2190064.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2181686-2190064/2181686-2190064.ipynb) [(video)](https://www.youtube.com/watch?v=qNxV4ejTsOk) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2181686-2190064.pdf)

---

## Py-Kids <a name="proy8"></a>

**Autores:**
**Angie Julieth Fuentes Barragan, Carlos Daniel Sanjuan Argote, Paula Andrea Castro Mendoza**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2172011-2172714-2171998.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2172011-2172714-2171998/2172011-2172714-2171998.ipynb) [(video)](https://www.youtube.com/watch?v=dMg3HE6-jks) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2172011-2172714-2171998.pdf)

---

## Diseño de laberintos a partir de autómatas deterministas <a name="proy9"></a>

**Autores:**
**Cristian Eduardo Rojas Pedraza, William David Romero Serrano, Yurieth Najhery Soler Maldonado**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2155505-2182696-2151090.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2155505-2182696-2151090/2155505-2182696-2151090.ipynb) [(video)](https://www.youtube.com/watch?v=59w8Hc3KfsQ) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2155505-2182696-2151090.pdf)

---

## MODELADO DE RUTAS PRESTADAS POR LA EMPRESA DE TRANSPORTE INTERMUNICIPAL BERLINAS DEL FONCE DESDE LA TERMINAL DE TRANSPORTES DE LA CIUDAD DE BUCARAMANGA <a name="proy10"></a>

**Autores:**
**Jose Alejandro Carrillo, Neyder Fabian Mosquera**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2182046-2182078.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2182046-2182078/2182046-2182078.ipynb) [(video)](https://www.youtube.com/watch?v=tZCwyJ1-biY) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2182046-2182078.pdf)

---

## AFD para la simulación de un cajero electrónico <a name="proy11"></a>

**Autores:**
**Nicolas Jaimes, Mauricio Romero, Daniel Baez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2182815-2182049-2182688.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2182815-2182049-2182688/2182815-2182049-2182688.ipynb) [(video)](https://www.youtube.com/watch?v=90BK7V4NlII) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2182815-2182049-2182688.pdf)

---

## Maquina expendedoras de cerveza <a name="proy12"></a>

**Autores:**
**Rances Rodrigues, Jorge Sandoval**

<!--<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2182028-2182052.jpg" style="width:700px;" />-->

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2182028-2182052/2182028-2182052.ipynb) [(video)](https://www.youtube.com/watch?v=OIgUEe_ZzNQ) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2182028-2182052.pdf)

---

## AFD QUE CLASIFICA EL TIPO DE ETS SEGÚN SÍNTOMAS <a name="proy13"></a>

**Autores:**
**Yaire Catalina López Santana, Dubian Enrique Palacios Rivera, Maria Paula Rodriguez Jerez**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2182061-2182063-2152012.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2182061-2182063-2152012/2182061-2182063-2152012.ipynb) [(video)](https://www.youtube.com/watch?v=vVgYrNJaCAs) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2182061-2182063-2152012.pdf)

---

## LINEA DE ATENCION AL CLIENTE PARA SERVICIOS MOVILES <a name="proy14"></a>

**Autores:**
**Jorge Cárdenas Parada, Isnardo Corredor Ariza, Daniel Torres Salcedo**

<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2182060-2182048-2182029.jpg" style="width:700px;" />

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2182060-2182048-2182029/2182060-2182048-2182029.ipynb) [(video)](https://www.youtube.com/watch?v=GcflmGR05xQ) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2182060-2182048-2182029.pdf)

---

## Automata de seguridad para conjunto residencial <a name="proy15"></a>

**Autores:**
**Julián Andrés García Roa, Juan Camilo Pertuz Manosalva**

<!--<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/raw/master/projects/img/2020-1/2171449-2160032.jpg" style="width:700px;" />-->

[(code)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/code/2020-1/2171449-2160032/2171449-2160032.ipynb) [(video)](https://www.youtube.com/watch?v=ROM2ERmF_JE) [(+info)](https://gitlab.com/bivl2ab/academico/cursos-uis/automatas/automatas-uis-student/-/blob/master/projects/pdf/2020-1/2171449-2160032.pdf)

---
