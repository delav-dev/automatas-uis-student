import sys
import os
import random
from colorama import Fore

#variables Globales
cartas = ['Duke','Asesina','Pirata','Condesa']
PlayerCarta1 = str
PlayerCarta2 = str
Cpu_Carta1 = str
Cpu_Carta2 = str
Cpu_cant_Cartas = int
Cpu_monedas = 0
cant_cartas = int #player
monedas = 0 # player
Lenguaje = ''



def JugarCoup ():
    global cartas,PlayerCarta1,PlayerCarta2,monedas,Cpu_cant_Cartas,Cpu_Carta1,Cpu_Carta2,Cpu_monedas,Lenguaje,cant_cartas 
    
    PlayerCarta1 = cartas[random.randrange(0,4)]#generando aletoriamente mis cartas
    PlayerCarta2 = cartas[random.randrange(0,4)] #generando aletoriamente mis cartas 
    cant_cartas = 2
    #cartas de la CPU
    Cpu_Carta1 = cartas[random.randrange(0,4)]
    Cpu_Carta2 = cartas[random.randrange(0,4)]
    Cpu_cant_Cartas = 2
    

    print('\n\n las eleciones posibles son ',cartas,'[ T1 , M ]')
    print('Para decir debes dijitar el primer caracter del nombre de la carta')
    print('Tus cartas : ')
    print('█ '+PlayerCarta1 + '  █ '+PlayerCarta2 )
    print('Ⓢ ',monedas )
    print('CPU █ '+Cpu_Carta1 + '  █ '+Cpu_Carta2 )

    while cant_cartas > 0 and Cpu_cant_Cartas > 0:
        if (monedas == 10):
            print('Posees Ⓢ10 monedas debes matar ')
            x = 'M'
        else:
            
            x = (input(Fore.BLUE+'decision : '+Fore.LIGHTWHITE_EX))

        x.upper()#las deciciones en mayuscula

        if x == 'D' or x == 'A' or x == 'P'  or x == 'T1' or (x == 'M' and monedas == 10):
            if Mi_Turno(x) == True:
                #decicion de la CPU
                if cant_cartas > 0 and Cpu_cant_Cartas > 0:
                    Cpu_turno()
                else:
                    break #probando esto
            else:
                print('No puedes Realizar esta accion , Sigue las REGLAS')
        else:
            print('ingrese un valor admitido, y Sigue las Reglas')

        str1 ='█ '+PlayerCarta1 + '  █ '+PlayerCarta2
        str2 = 'Ⓢ '+str(monedas)
        print(str1.rjust(2, '-') )
        print(Fore.YELLOW +str2.rjust(2, '-') +Fore.LIGHTWHITE_EX)
        #clear()#limpiar consola
         
    print(Fore.GREEN+'Finalizo la partida ')
    print('PLayer ',(cant_cartas),'█ cartas' )
    print('CPU ', Cpu_cant_Cartas,'█ cartas')
    if cant_cartas >0:
        print(' has ganado la partida ')
        print('la palabra es:')
        print(Lenguaje)
    else:
        print(' has perdido la partida ')
        print('la palabra es:')
        print(Lenguaje)

    
def Mi_Turno(x):
    global cartas,PlayerCarta1,PlayerCarta2,monedas,Cpu_cant_Cartas,Cpu_Carta1,Cpu_Carta2,Cpu_monedas,Lenguaje,cant_cartas
    if x == 'D' and monedas <= 7:#no puedo coger si tengo mas de 7 monedas
        Lenguaje += ' D'
        if random.randrange(0,2) == 0:#cero significa que no le cree
            print('CPU: "NO TE CREO" ')
            Lenguaje += ' NC'
            if PlayerCarta1 == 'Duke' or PlayerCarta2 == 'Duke':
                Cpu_PerderCarta()
                monedas +=3
                print('la maquina a perdido 1 carta')
            else:
                print('Pierdes una carta')
                PerderCarta()
        else:
            print('CPU: "TE CREO"')
            Lenguaje += ' SC'
            monedas += 3
    else :
        if x == 'D':
            return False

    if x == 'A' and monedas >=3:#pago 3 Ⓢ y le mata una carta al contrincante
        Lenguaje += ' '+x
        monedas -= 3
        if Cpu_Carta1 == cartas[3] or Cpu_Carta2 == cartas[3]:               #si la CPU tiene la condesa
            AccionCondesa()
        else:                                                   #la CPU No tiene la condesa            
            d = random.randrange(0,3)                           
            if d == 0:                                          #La CPU no me cree
                print('CPU: "NO TE CREO" ')
                Lenguaje += ' NC'
                if PlayerCarta1  or PlayerCarta2 == cartas[1]:  #tengo la carta , la CPU pierde 2 cartas
                    Cpu_PerderCarta()
                    Cpu_PerderCarta()
                    print('la maquina a perdido 2 carta')
                else:                                           #no tengo la carta
                    PerderCarta()
                    print('Has perdido una carta')
            elif d == 1:                                        #la CPU decide mentirme
                AccionCondesa()
            elif d == 2:                                        #La CPU me cree
                print('CPU: "TE CREO" ')  
                Cpu_PerderCarta()                               # la cpu pierde una carta              
    else:
        if x == 'A':
            return False    

    if x == 'P' and monedas < 9 and Cpu_monedas >= 2:           #robo 2
        Lenguaje += ' P'
        if Cpu_Carta1 == cartas[2] or Cpu_Carta2 == cartas[2]:  #si la cpu tiene la carta pirata
            AccionPirata()
        else:
            d = random.randrange(0,3)
            if d == 1:                                          #la CPU decide mentir puesto que no tiene la carta  
                AccionPirata()
            elif d == 0:                                        #la CPU decide NO mentirme
                print('CPU: "SI TE CREO"')
                Lenguaje += ' SC'
                Cpu_monedas -= 2
                monedas += 2
            elif d == 2:                                        #la CPU NO me cree
                print('CPU: "NO TE CREO"')
                Lenguaje += ' NC'              
                if PlayerCarta1 or PlayerCarta2 == cartas[2]:   # si tengo la carta 
                    print('la cpu pierde una carta')
                    Cpu_PerderCarta()
                    monedas += 2
                    Cpu_monedas -= 2
                else:                                           #si no tengo la carta 
                    PerderCarta()
    else:
        if x == 'P':
            return False

    if x == 'T1':
        Lenguaje += ' T1'
        monedas += 1
        
    if x == 'M':
        Lenguaje += ' M'
        monedas -= 10
        Cpu_PerderCarta()
        

    return True

def AccionCondesa():
    global cartas,PlayerCarta1,PlayerCarta2,monedas,Cpu_cant_Cartas,Cpu_Carta1,Cpu_Carta2,Cpu_monedas,Lenguaje,cant_cartas
    print('CPU : "NO PUEDES PORQUE SOY LA CONDESA "')#NP
    Lenguaje += ' NP'
    if input('Le Crees? (SC/NC)').upper() == 'NC':  #no le creo que tenga la condesa
        if Cpu_Carta1 == cartas[3] or Cpu_Carta2 == cartas[3]:   #si la maquina tiene la condesa
            PerderCarta()
        else:                                       #si la CPU NO tiene la condesa
            Cpu_PerderCarta()
            Cpu_PerderCarta()
    else:                                           #decido creerle
        Lenguaje +=' SC'

def AccionPirata():
    global cartas,PlayerCarta1,PlayerCarta2,monedas,Cpu_cant_Cartas,Cpu_Carta1,Cpu_Carta2,Cpu_monedas,Lenguaje,cant_cartas
    print('CPU : "NO PUEDES PORQUE SOY EL PIRATA"')#NP
    Lenguaje += ' NP'
                                                    #el player decide creerle o no
    if input('Le Crees? (SC/NC)').lower() == 'nc':  #no le creo que tenga al Pirata
        if Cpu_Carta1 == cartas[2] or Cpu_Carta2 == cartas[2]:   #si la maquina tiene al Pirata
            PerderCarta()
        else:                                       #si la CPU NO tiene la carta
            monedas += 2
            Cpu_monedas -= 2
            Cpu_PerderCarta()
    else:                                           #decido creerle
        Lenguaje +=' SC'

def PerderCarta ():#esta funcion es para cuando el player pierde una carta
    global cartas,PlayerCarta1,PlayerCarta2,monedas,Cpu_cant_Cartas,Cpu_Carta1,Cpu_Carta2,Cpu_monedas,Lenguaje,cant_cartas
    if cant_cartas > 0:
        cant_cartas -= 1
        Lenguaje += ' -1P'
    x = int(input('Cual carta desea perder? (1 or 2)'))
    if x == 1:
        if PlayerCarta1 == '':
            PlayerCarta2 = ''
        else:
            PlayerCarta1 = ''
    elif x == 2:
        if PlayerCarta2 == '':
            PlayerCarta1 == ''
        else:
            PlayerCarta2 =''
    else:#en caso de que no haya decidido bien o haya querido perder una carta cualquiera
        if random.randrange(0,2) == 0:
            PlayerCarta1 = ''
        else:
            PlayerCarta2 = ''


def Cpu_PerderCarta():# es una funcion que decide aleatoriemente cual carte debe perder la CPU por una mala decision 
    global cartas,PlayerCarta1,PlayerCarta2,monedas,Cpu_cant_Cartas,Cpu_Carta1,Cpu_Carta2,Cpu_monedas,Lenguaje,cant_cartas
    if Cpu_cant_Cartas > 1:
        if random.randrange(0,2) == 0:
            Cpu_Carta1 = ''
        else:
            Cpu_Carta2 = ''
    else:
        Cpu_Carta1 = ''
        Cpu_Carta2 = ''
    Lenguaje += ' -1C'
    if Cpu_cant_Cartas>0:#para que no haga algo como -1 cartas
        Cpu_cant_Cartas -= 1

def Cpu_turno():
    global cartas,PlayerCarta1,PlayerCarta2,monedas,Cpu_cant_Cartas,Cpu_Carta1,Cpu_Carta2,Cpu_monedas,Lenguaje,cant_cartas
    decisiones = ['D','A','T1','P']
    print('---------TURNO DE LA CPU')
    if Cpu_monedas == 10:
        Lenguaje += ' M'
        print('la maquina te mata una carta puesto que tien 10 monedas')
        PerderCarta()
    else:
        exit = True
        while exit:#la variable exit es un booleano utilizado para que la maquina cuando decida una accion que no puede realizar ,
                    # no pierda el turno y vuelva a decidir
            exit=False
            x = decisiones[random.randrange(0,4)]
            if x == 'D' and Cpu_monedas <= 7 :
                print('CPU: "Cojo 3 porque soy el duke"')
                Lenguaje += ' D'
                if input('Le Cree? (SC/NC)').upper() == 'NC':   #NC significa que no le creo
                    Lenguaje += ' NC'
                    if Cpu_Carta1 == cartas[0] or Cpu_Carta2 == cartas[0]:   #si la CPU tiene al duke
                        print ('la CPU si tiene al Duke')
                        PerderCarta()
                        Cpu_monedas +=3
                    else:                                       #si la CPU NO tiene al duke
                        print (Fore.RED+'La CPU pierde una carta'+Fore.LIGHTWHITE_EX)
                        Cpu_PerderCarta()
                else:
                    Cpu_monedas += 3
            elif x == 'D':
                exit = True

            if x == 'A' and Cpu_monedas >=3:                            #CPU paga 3 Ⓢ y me mata una carta 
                Lenguaje += ' A'
                Cpu_monedas -= 3
                print ('CPU : "soy la asesina te mato una carta"')
                if PlayerCarta1 == cartas[3] or PlayerCarta2 == cartas[3]:           #si el Player tiene la condesa 
                    print('Player: no puedes porque tengo la condesa')
                    Lenguaje +=' NP'
                    d = random.randrange(0,2)                           #la CPU decide si Creerte 
                    if d == 0:                                          #La CPU no te cree
                            print('CPU: "NO TE CREO" ')
                            Lenguaje += ' NC'
                            print('la maquina a perdido 1 carta')
                            Cpu_PerderCarta()  
                    elif d == 1:                                        #La CPU te cree
                        print('CPU: "TE CREO" ')  
                        Lenguaje += ' SC'
                else:
                    d = input('Mentir: tengo la condesa (NP) o creerle (SC/NC)?')
                    if d == 'NP':                                   #le mientes
                        Lenguaje +=' NP'
                        cd = random.randrange(0,2)                           #la CPU decide si Creerte 
                        if cd == 0:                                          #La CPU no te cree
                            print('CPU: "NO TE CREO" ')
                            Lenguaje += ' NC'
                            print('pierdes 2 cartas')
                            PerderCarta()
                            PerderCarta() 
                        else:                                        #La CPU te cree
                            print('CPU: "TE CREO" ')  
                            Lenguaje += ' SC'
                    elif d == 'NC':                                         #no le crees que tenga la asesina
                        Lenguaje += ' NC'
                        if Cpu_Carta1 == cartas[1] or Cpu_Carta2 == cartas[1]:
                            print('pierdes 2 cartas')
                            PerderCarta()
                            PerderCarta()
                        else:
                            Cpu_PerderCarta()
                    else:                                                      #le crees a la CPU y te mata una carta      
                        print('le crees a la CPU ')  
                        Lenguaje += ' SC'
                        PerderCarta()        
            else:
                if x == 'A':
                    exit =True    

            if x == 'P' and Cpu_monedas < 9 and monedas >= 2:               #la CPU me roba 2
                Lenguaje += ' P'
                print('CPU: "Te robo 2 porque soy el Pirata"')
                if PlayerCarta1 == cartas[2] or PlayerCarta2 == cartas[2]:           #si el Player tiene al Pirata 
                    print('Player: no puedes porque tengo al pirata')
                    Lenguaje +=' NP'
                    d = random.randrange(0,2)                           #la CPU decide si Creerte 
                    if d == 0:                                          #La CPU no te cree
                            print('CPU: "NO TE CREO" ')
                            Lenguaje += ' NC'
                            print('la CPU pierde 1 carta')
                            Cpu_PerderCarta()  
                    elif d == 1:                                        #La CPU te cree
                        print('CPU: "TE CREO" ')  
                        Lenguaje += ' SC'
                else:                                                       #el player no tiene al Pirata
                    d = input('Mentir: tengo el Pirata (NP) o creerle (SC/NC)?')
                    if d == 'NP':                                           #le mientes
                        Lenguaje +=' NP'
                        cd = random.randrange(0,2)                           #la CPU decide si Creerte o no
                        if cd == 0:                                          #La CPU no te cree
                            print('CPU: "NO TE CREO" ')
                            Lenguaje += ' NC'
                            print('pierdes 1 cartas y 2 monedas')
                            PerderCarta()
                            monedas -=2
                            Cpu_monedas += 2
                        else:                                               #La CPU te cree
                            print('CPU: "TE CREO" ')  
                            Lenguaje += ' SC'
                    elif d == 'NC':                                         #no le crees que tenga el Pirata
                        Lenguaje += ' NC'
                        if Cpu_Carta1  == cartas[2] or Cpu_Carta2 == cartas[2]:           #si la CPU tiene al Pirata
                            print('pierdes 1 cartas y 2 monedas')
                            PerderCarta()
                            monedas -=2
                            Cpu_monedas += 2
                        else:                                               #si la CPU no tiene al Pirata
                            Cpu_PerderCarta()
                    else:                                                   #le crees a la CPU y te roba 2 monedas      
                        print('le crees a la CPU ')  
                        monedas -=2
                        Cpu_monedas += 2
            elif x == 'P':
                exit = True

            if x == 'T1':
                print('CPU: "COJO 1"')
                Lenguaje += ' T1'
                Cpu_monedas += 1



